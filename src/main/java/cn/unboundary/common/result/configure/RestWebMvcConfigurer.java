package cn.unboundary.common.result.configure;

import cn.hutool.core.date.DatePattern;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 自定义一些Handler，Interceptor，ViewResolver，MessageConverter
 */
public class RestWebMvcConfigurer implements WebMvcConfigurer {
    /**
     * 注册自定义的Formatter和Convert,例如, 对于日期类型,枚举类型的转化.
     */
    @Override
    public final void addFormatters(FormatterRegistry registry) {
        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
        registrar.setTimeFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN));
        registrar.setDateFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN));
        registrar.setDateTimeFormatter(DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN));
        this.restAddFormatters(registry);
        registrar.registerFormatters(registry);
    }

    /**
     * 信息转换器配置
     * @param converters 信息转换器列表
     */
    @Override
    public final void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.removeIf(converter -> converter.getClass() == StringHttpMessageConverter.class);
        this.restConfigureMessageConverters(converters);
    }

    /**
     * 自定义Formatter和Convert
     * @param registry 注册器
     */
    protected void restAddFormatters(FormatterRegistry registry) {

    }

    /**
     * 自定义信息转换器
     * @param converters 信息转换器列表
     */
    protected void restConfigureMessageConverters(List<HttpMessageConverter<?>> converters) {

    }
}