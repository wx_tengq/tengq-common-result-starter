package cn.unboundary.common.result.entity;

import cn.unboundary.common.result.configure.jackson.NullObjectToEmptyObjectSerializer;
import cn.unboundary.common.result.enums.ResultCode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 统一响应体
 * @param <T>
 */
@Data
public class RestResponse<T> implements Serializable {
    private static final long serialVersionUID = -168658541097740143L;
    private Integer code;
    private String msg;
    @JsonSerialize(nullsUsing = NullObjectToEmptyObjectSerializer.class)
    private T data;

    private RestResponse(ResultCode resultCode, T data) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.data = data;
    }

    private RestResponse(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static RestResponse<Object> ok() {
        return new RestResponse<>(ResultCode.SUCCESS, null);
    }

    public static RestResponse<Object> ok(String msg) {
        return new RestResponse<>(ResultCode.SUCCESS.getCode(), msg, null);
    }

    public static <T> RestResponse<T> ok(T data) {
        return new RestResponse<>(ResultCode.SUCCESS, data);
    }

    public static <T> RestResponse<T> ok(String msg, T data) {
        return new RestResponse<T>(ResultCode.SUCCESS.getCode(), msg, data);
    }

    public static RestResponse<Object> failed(String msg) {
        return new RestResponse<>(ResultCode.FAILED.getCode(), msg, null);
    }

    public static RestResponse<Object> failed(Integer code, String msg) {
        return new RestResponse<>(code, msg, null);
    }

    public static RestResponse<Object> failed(ResultCode resultCode) {
        return new RestResponse<>(resultCode, null);
    }

    public static <T> RestResponse<T> failed(ResultCode resultCode, T data) {
        return new RestResponse<>(resultCode, data);
    }
}
