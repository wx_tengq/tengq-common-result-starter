package cn.unboundary.common.result.annotation;

import java.lang.annotation.*;

/**
 * 不进行统一结果封装标识
 */
@Documented
@Inherited
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreRestBody {
}
