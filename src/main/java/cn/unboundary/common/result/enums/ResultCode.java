package cn.unboundary.common.result.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Api状态码枚举
 */
@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(200, "请求成功！"),
    BODY_NOT_MATCH(400, "请求的数据格式不符！"),
    UNAUTHORIZED(401, "尚未登录，请先登录！"),
    REPEAT_LOGIN(402, "你已在另一台设备登录，本次登录已下线！"),
    FORBIDDEN(403, "没有相关没有权限！"),
    NOT_FOUND(404, "未找到该资源！"),
    REQUEST_METHOD_NOT_SUPPORT(405, "不支持此类型请求！"),
    REQUEST_PARAMETER_ERROR(406, "请求参数有误"),
    FAILED(424, "请求失败！"),
    INTERNAL_SERVER_ERROR(500, "服务器内部错误！"),
    SERVER_BUSY(503, "服务器正忙，请稍后再试！");

    private final Integer code;
    private final String msg;
}
