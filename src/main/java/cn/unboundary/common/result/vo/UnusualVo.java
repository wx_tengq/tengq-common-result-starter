package cn.unboundary.common.result.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 返回值为基本数据类型和数组包装类
 */
@Data
@NoArgsConstructor
public class UnusualVo<T> implements Serializable {
    /**
     * 数据
     */
    private T data;

    public UnusualVo(T data) {
        this.data = data;
    }
}

