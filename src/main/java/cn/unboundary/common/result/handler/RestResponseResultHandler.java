package cn.unboundary.common.result.handler;

import cn.unboundary.common.result.annotation.IgnoreRestBody;
import cn.unboundary.common.result.entity.RestResponse;
import cn.unboundary.common.result.vo.UnusualVo;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Collection;

/**
 * 统一响应结果处理器
 */
public class RestResponseResultHandler implements ResponseBodyAdvice<Object> {

    @Override
    public final boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> clazz) {
        return !(methodParameter.hasMethodAnnotation(IgnoreRestBody.class) || methodParameter.getParameterType().isAssignableFrom(RestResponse.class));
    }

    @Override
    public final Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> clazz, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 返回值为String类型时将结果当作msg进行处理
        if (body instanceof String)
            return RestResponse.ok(body.toString());
        else if (body == null) // 没有返回值当成功状态进行处理
            return RestResponse.ok();
        else if (body instanceof  Boolean || body instanceof Number || body instanceof Collection || body.getClass().isArray())
            return RestResponse.ok(new UnusualVo<>(body));
        return RestResponse.ok(body);
    }
}
